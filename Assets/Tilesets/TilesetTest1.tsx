<?xml version="1.0" encoding="UTF-8"?>
<tileset name="TilesetTest1" tilewidth="16" tileheight="16" spacing="1" tilecount="1920" columns="57">
 <image source="../Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Grass" tile="5"/>
  <terrain name="Water" tile="0"/>
  <terrain name="Dirt" tile="578"/>
 </terraintypes>
 <tile id="0" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="2" terrain="0,0,0,1">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="3" terrain="0,0,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4" terrain="0,0,1,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5" terrain="0,0,0,0"/>
 <tile id="57" terrain="1,1,1,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="58" terrain="1,1,0,1">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="59" terrain="0,1,0,1">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="60" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="61" terrain="1,0,1,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="62" terrain="0,0,0,0"/>
 <tile id="114" terrain="1,0,1,1">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="115" terrain="0,1,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="116" terrain="0,1,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="117" terrain="1,1,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="118" terrain="1,0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="285">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="518" terrain="2,2,2,"/>
 <tile id="519" terrain="2,2,,2"/>
 <tile id="520" terrain=",,,2"/>
 <tile id="521" terrain=",,2,2"/>
 <tile id="522" terrain=",,2,"/>
 <tile id="575" terrain="2,,2,2"/>
 <tile id="576" terrain=",2,2,2"/>
 <tile id="577" terrain=",2,,2"/>
 <tile id="578" terrain="2,2,2,2"/>
 <tile id="579" terrain="2,,2,"/>
 <tile id="634" terrain=",2,,"/>
 <tile id="635" terrain="2,2,,"/>
 <tile id="636" terrain="2,,,"/>
 <tile id="1698">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
